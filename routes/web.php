<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/master', 'IndexController@home');

Route::get('/register', 'AuthController@bio');

Route::post('/welcome','AuthController@signup');

Route::get('/data-table', function(){
    return view('table.data-table');
});