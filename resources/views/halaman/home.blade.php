@extends('layout.master')

@section('judul')
    Halaman Home
@endsection

@section('content')
<h1>Media Online</h1>

<h2>Social Media Developer</h2>

<p>Belajar dan berbagi agar hidup menjadi lebih baik</p>

<h3>Benefit Join di Media Online</h3>

<ul>
    <li>Mendapatkan motivasi dari sesama developer</li>
    <li>Sharing Knowledge</li>
    <li>Dibuat oleh calon developer terbaik</li>
</ul>

<h3>Cara Bergabung ke Media Online</h3>

<ol>
    <li>Mengunjungi Website ini</li>
    <li>Mendaftarkan di <a href="Form Sign Up.html">Form Sign Up</a> </li>
    <li>Selesai</li>

</ol>

@endsection