<!DOCTYPE html>
<html>
<head>
<title>PKS Digital Schools</title>
</head>
<body>

<h1>Buat Account Baru</h1>

<h2>Sign Up Form</h2>

<form action="/welcome" method="post">
    @csrf
    <label for="fname">First Name</label><br>
    <br>
    <input type="text" id="fname" name="fname"><br>
    <br>
    <label for="lname">Last Name</label>
    <br>
    <input type="text" id="lname" name="lname"><br>
    <br>

    <p>Gender :</p>

    <input type="radio" id="male" name="gender" value="Male">
    <label for="male">Male</label><br>
    <input type="radio" id="female" name="gender" value="Female">
    <label for="female">Female</label><br>
    <input type="radio" name="gender" id="Other">
    <label for="other">Other</label><br>
    <br>

    <p>Nationality :</p>
    <br>

    <select name="nationality" id="nation">
        <option value="Indonesia">Indonesia</option>
        <option value="Amerika">Amerika</option>
        <option value="Inggris">Inggris</option>
    </select><br>

    <p>Language Spoken :</p>

    <input type="checkbox" id="bahasa1" name="bahasa1" value="Indonesia">
    <label for="bahasa1">Bahasa Indonesia</label><br>
    <input type="checkbox" id="bahasa2" name="bahasa2" value="English">
    <label for="bahasa2">English</label><br>
    <input type="checkbox" name="bahasa3" id="bahasa3" value="Other">
    <label for="bahasa3">Other</label><br>

    <p>Bio :</p><br>

    <textarea name="biodata" id="bio" cols="30" rows="10"></textarea><br>
    <input type="submit" value="Sign Up">



</form>


</body>
</html>