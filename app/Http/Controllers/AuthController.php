<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function bio(){
        return view('halaman.register');
    }

    public function signup(Request $request){
        $firstname = $request['fname'];
        $lastname = $request['lname'];
        return view('/welcome',compact('firstname','lastname'));
    }
}
